package AutomationTestSystem.Base;

import AutomationTestSystem.Service.BaseRunService;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import io.appium.java_client.android.AndroidDriver;

/**
 * <br>对应于XML文件中的Case元素</br>
 *
 * @version 1.0
 * @since   1.0
 */
public class TestBase {
	
	private String id;

    private String name;

    private boolean cancel;

    private WebDriver Wdriver;

    /**
     * 执行结果：0=失败，1=成功，-1=待处理
     */
    private int result = -1;

    /**
     * 配置的默认 隐式等待（毫秒），如无则使用
     * 全局的,建议新页面第一个要素可以额外增加点时间，其他的不需要
     */
    private Integer implicitlyWait;
    
    private AndroidDriver<WebElement> Adriver;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCancel() {
        return cancel;
    }

    public void setCancel(boolean cancel) {
        this.cancel = cancel;
    }
    
    public WebDriver getWebDriver() {
        return Wdriver;
    }

    public void setWebDriver(WebDriver driver) {
        this.Wdriver = driver;
    }
    
    public AndroidDriver<WebElement> getAndroidDriver() {
        return Adriver;
    }

    public void setAndroidDriver(AndroidDriver<WebElement> driver) {
        this.Adriver = driver;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }


    public Integer getImplicitlyWait() {
        return implicitlyWait;
    }

    public void setImplicitlyWait(Integer implicitlyWait) {
        this.implicitlyWait = implicitlyWait;
    }

    /**
     * 最终隐式等待时间（ms）
     * @return
     */
    public Integer getFinalImplicitlyWait() {
        if(implicitlyWait == null || implicitlyWait <= 0){
            return BaseRunService.implicitlyWait*1000;
        }else
            return implicitlyWait;
    }
}
