package AutomationTestSystem.Util;

import java.io.*;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import AutomationTestSystem.exception.SystemException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Slf4j
public class ConfigUtil {

    private static Logger LOG = LoggerFactory.getLogger(ConfigUtil.class);

    private static Map<String, Properties> instanceMap = new ConcurrentHashMap<>();

    /**
     * 默认的配合加载路径
     */
    public static String DEFAULT_CONFIG_PATH = null;

    public static String getProperty(String key, String config){
        return getProperty(key,config,null);
    }

    public static String getProperty(String key, String config,String path){
        if(key == null || "".equals(key)){
            log.error("请传入正确的key信息");
            return null;
        }
            
        if(config == null || "".equals(config)){
            log.error("请传人正确的配置文件信息");
            return null;
        }
        Properties p = instanceMap.get(config);
        if(p != null){
            return p.getProperty(key);
        }else{
            return loadConfig(config,path).getProperty(key);
        }

    }

    /**
     * 优先加载class下的文件，找不到则加载jar路径下
     * @param config
     * @param path 指定加载路径，如无则class路径下加载
     * @return
     */
    private static Properties loadConfig(String config,String path){
        Properties p = new Properties();
        try {
            InputStream in = null;
                if(path != null){
                    log.info("开始从外部:{} 加载配置！",path);
                    File file = new File(path);
                    if(!file.exists()){
                        throw new SystemException("无配置文件！");
                    }
                    p.load(in);
                }else{
                    in  = ConfigUtil.class.getClassLoader().getResourceAsStream(config);
                    p.load(in);
                }


        } catch (FileNotFoundException e) {
            log.error("配置文件加载失败", e.fillInStackTrace());
        } catch (IOException e) {
             log.error("配置文件加载失败", e.fillInStackTrace());
         }

        instanceMap.put(config,p);

        return p;
    }
    
    public static void main(String[] args) {
       // String value = ConfigUtil.getProperty("image.maxSize", Constants.CONFIG_COMMON);
        String value = ConfigUtil.getProperty("image.maxSize", "appium_common2.properties");

        System.out.println(value);
    }
}
