package AutomationTestSystem.Util;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import AutomationTestSystem.exception.DbException;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.util.JdbcUtils;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;

/**
 * 数据库操作类
 */
@Slf4j
@SuppressWarnings("unused")
public class DBHelper {


    /**
     * 存储多个DB连接池
     */
    private static Map<Integer,DruidDataSource> dataSources = new ConcurrentHashMap();

    /**
     * 数据库插入
     * @param dbIndex 支持null 或者 1,2,3,4
     * @param sql
     * @return
     */
    public static int insert(Integer dbIndex,String sql){
        return executeUpdate(sql, OpType.INSERT,dbIndex);
    }

    
    /**
     * 数据库删除
     * @param dbIndex 支持null 或者 1,2,3,4
     * @param sql
     * @p
     * @return
     */
    public static int delete(Integer dbIndex,String sql){
        return executeUpdate(sql, OpType.DELETE,dbIndex);
    }

    
    /**
     * 数据库修改
     * @param dbIndex 支持null 或者 1,2,3,4
     * @param sql
     * @return
     */
    public static int update(Integer dbIndex,String sql){
        return executeUpdate(sql, OpType.UPDATE,dbIndex);
    }
    

    
    /**
     * 数据库查询
     * @param dbIndex 支持null 或者 1,2,3,4
     * @param sql
     * @return
     */
    public static List<Map<String, Object>> query(Integer dbIndex,String sql) {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();
        log.info("DB序号：{} Sql:{}" ,(dbIndex == null)?-1:dbIndex, sql);
        try {
            DruidDataSource dataSource = generalCheckDataSource(dbIndex);
            connection = dataSource.getConnection();

            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            ResultSetMetaData metaData = rs.getMetaData();
            while(rs.next()){
                int count = metaData.getColumnCount();
                Map<String, Object> rowMap = new HashMap<String, Object>();
                for(int i=1; i<=count; i++){    	
                    rowMap.put(metaData.getColumnLabel(i), rs.getObject(i));
//                    System.out.print(rs.getString(i) + "\t");
                    if ((i == 2) && (rs.getString(i).length() < 8)) {
//                        System.out.print("\t");
                    }
                }
//                System.out.println("");
                results.add(rowMap);
            }
            log.info("Count: " + results.size());
        } catch (SQLException e) {

            log.error("查询失败：{}", e.fillInStackTrace());
            throw new DbException(e);
        }catch (Exception e2){
            log.error("查询异常：{}", e2.fillInStackTrace());

            throw new DbException(e2);
        } finally {
            close(connection,ps, rs);
        }
        return results;
    }


    
	/**
     * 指定SQL语句,执行查询操作,并打印结果
     * @param sql
     * @return
     */
	/**
    public static void Query(String SQL) {  
	  	checkConnection();
	  	PreparedStatement ps = null;
        ResultSet rs = null;
	        try {
	              ps = connection.prepareStatement(SQL);
	              rs = ps.executeQuery();
	              int col = rs.getMetaData().getColumnCount(); 
	              System.out.println("============================");
	              while (rs.next()) {
	                   for (int i = 1; i <= col; i++) {
	                        System.out.print(rs.getString(i) + "\t");
	                      if ((i == 2) && (rs.getString(i).length() < 8)) {
	                             System.out.print("\t");
	                      }
	                   }
	                System.out.println("");
	              }
	                System.out.println("============================");
	                close();
	  	    }
	        catch (SQLException e) {
	        	System.out.println("查询失败");
	            e.printStackTrace();
	        }
	}
     **/
    
    /**
     * 执行存储过程,带参数
     * @param dbIndex 支持null 或者 1,2,3,4
     * @param prc_name
     * @param params
     * @return
     */
    public static int procedure(Integer dbIndex,String prc_name, Object... params) {
        Connection connection = null;
        CallableStatement cs = null;
        try {
            DruidDataSource dataSource = generalCheckDataSource(dbIndex);
            connection = dataSource.getConnection();

            cs = connection.prepareCall(prc_name);
            if(params != null && params.length > 0){
                for(int i=0; i<params.length; i++){
                	//TODO set类型对应数据库,包含输入和输出
                    cs.setString(i+1, String.valueOf(params[i]));
                }
            }
            log.info("开始执行存储过程: "+ prc_name); 
            cs.execute();
            log.info("存储过程执行成功 "); 
        } catch (SQLException e) {

            log.error("DB：{} 存储过程["+prc_name+"]执行失败",((dbIndex == null)?-1:dbIndex), e.fillInStackTrace());
            throw new DbException(e);
        }catch (Exception e2){


            log.error("DB：{}存储过程["+prc_name+"]执行异常",((dbIndex == null)?-1:dbIndex), e2.fillInStackTrace());

            throw new DbException(e2);
        } finally {
            try {
                if(cs != null){ cs.close(); cs = null;}
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if(connection != null){
                    connection.close();
                    connection = null;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
		return 0;   
    }
    

    
    /**
     * 执行数据库操作
     * @param sql
     * @param type
     * @return
     */
    private static int executeUpdate(String sql, OpType type,Integer dbIndex){
        PreparedStatement ps = null;
        log.info("Sql: " + sql);
        Connection connection = null;
        try {
            DruidDataSource dataSource =  generalCheckDataSource(dbIndex);
            connection = dataSource.getConnection();
            ps = connection.prepareStatement(sql);
            int result = ps.executeUpdate();
            log.info("Result: " + result);
            return result;
        } catch (SQLException e) {
            log.info("DB：{} {} 失败",(dbIndex == null)?-1:dbIndex, sql, e.fillInStackTrace());

            throw new DbException(e);
        }catch (Exception e2){
            log.error("DB：{} {}异常：{}",((dbIndex == null)?-1:dbIndex),type.desc(), e2.fillInStackTrace());

            throw new DbException(e2);
        } finally {
            close(connection,ps,null);
        }

    }
    


    private static synchronized DruidDataSource generalCheckDataSource(Integer dbIndex) throws Exception{
        if(dbIndex == null )
            dbIndex = -1;
        DruidDataSource dataSource = dataSources.get(dbIndex);
        if(dataSource == null ){
            dataSource = generalInitConnections(dbIndex);
        }
        return dataSource;
    }



    private static DruidDataSource generalInitConnections(Integer dbIndex) throws Exception{
        String jdbcUrl;
        String userName;
        String password;
        if(dbIndex == null || dbIndex == -1) {
             jdbcUrl = ConfigUtil.getProperty(Constants.DB_URL_KEY, Constants.CONFIG_JDBC);
             userName = ConfigUtil.getProperty(Constants.DB_USERNAME_KEY, Constants.CONFIG_JDBC);
             password = ConfigUtil.getProperty(Constants.DB_PASWORD_KEY, Constants.CONFIG_JDBC);
        }else{
             jdbcUrl = ConfigUtil.getProperty(Constants.DB_URL_KEY+dbIndex, Constants.CONFIG_JDBC);
             userName = ConfigUtil.getProperty(Constants.DB_USERNAME_KEY+dbIndex, Constants.CONFIG_JDBC);
             password = ConfigUtil.getProperty(Constants.DB_PASWORD_KEY+dbIndex, Constants.CONFIG_JDBC);
        }

        if(StrUtil.isEmpty(jdbcUrl)) {
           // log.error("未配置DB连接地址");
            throw new Exception("未配置DB连接地址");
        }
        if(StrUtil.isEmpty(userName)){
            //log.error("未配置DB登陆名");
            throw new Exception("未配置DB登陆名");
        }
        if(StrUtil.isEmpty(password)){//好像允许空密码
            //log.error("未配置DB登陆密码");

            password = "";
        }

        String maxConnectStr = ConfigUtil.getProperty(Constants.DB_MAXCONNECT_KEY, Constants.CONFIG_JDBC);
        String initConnectStr = ConfigUtil.getProperty(Constants.DB_INITCONNECT_KEY, Constants.CONFIG_JDBC);

        int maxConnect = 10;
        int initConnect = 1;
        if(StrUtil.isNotEmpty(maxConnectStr) && NumberUtil.isInteger(maxConnectStr))
            maxConnect = Integer.parseInt(maxConnectStr);

        if(StrUtil.isNotEmpty(initConnectStr) && NumberUtil.isInteger(initConnectStr))
            initConnect = Integer.parseInt(initConnectStr);
        else
            initConnect = maxConnect/2;

        DruidDataSource dataSource = initDataSource(jdbcUrl,userName,password,maxConnect,initConnect);

        if (dbIndex == null)
            dbIndex = -1;
        dataSources.put(dbIndex,dataSource);

        return dataSource;
    }

    private static DruidDataSource initDataSource(String jdbcUrl,String userName,String password,Integer maxConnect,Integer initConnect) throws Exception{
        if(maxConnect == null)
            maxConnect = 10;
        if(initConnect == null)
            initConnect = maxConnect/2;

        String driverClassName = JdbcUtils.getDriverClassName(jdbcUrl);
        String dbType = JdbcUtils.getDbType(jdbcUrl,driverClassName);

        log.info("开始处理BD连接：{}，地址：{}",dbType,jdbcUrl);


        //数据源配置
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUrl(jdbcUrl);
        dataSource.setDriverClassName(driverClassName); //这个可以缺省的，会根据url自动识别
        dataSource.setUsername(userName);
        dataSource.setPassword(password);

        //下面都是可选的配置
        dataSource.setInitialSize(initConnect);  //初始连接数，默认0
        dataSource.setMaxActive(maxConnect);  //最大连接数，默认8
        //dataSource.setMinIdle(10);  //最小闲置数
        dataSource.setMaxWait(2000);  //获取连接的最大等待时间，单位毫秒
        dataSource.setPoolPreparedStatements(true); //缓存PreparedStatement，默认false
        dataSource.setMaxOpenPreparedStatements(20); //缓存PreparedStatement的最大数量，默认-1（不缓存）。大于0时会自动开启缓存PreparedStatement，所以可以省略上一句代码
        return dataSource;
    }


    /**
     * 关闭数据库
     */
    public static void close(Connection connection,PreparedStatement ps, ResultSet rs){
        try {
            if(rs != null){
                rs.close();
                rs = null;
            }
            if(ps != null){
                ps.close();
                ps = null;
            }
        } catch (SQLException e) {
            log.error("数据库异常", e.fillInStackTrace());
        }

        try {
            if(connection != null && !connection.isClosed())
                connection.close();
            connection = null;
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("数据库关闭失败");
        }
    }
    
    public static class MyUserInfo implements UserInfo {
 	   private String passphrase = null;

 	   public MyUserInfo(String passphrase) {
 	    this.passphrase = passphrase;
 	   }

 	   public String getPassphrase() {
 	    return passphrase;
 	   }

 	   public String getPassword() {
 	    return null;
 	   }

 	   public boolean promptPassphrase(String s) {
 	    return true;
 	   }

 	   public boolean promptPassword(String s) {
 	    return true;
 	   }

 	   public boolean promptYesNo(String s) {
 	    return true;
 	   }

 	   public void showMessage(String s) {
 	    System.out.println(s);
 	   }
    }
    
    /**
     * 数据库操作枚举
     */
    enum OpType {
        
        INSERT("插入"), 
        UPDATE("更新"), 
        DELETE("删除"), 
        QUERY("查询"), 
    	PROCEDURE("执行存储过程");
    	
        String desc;
        
        OpType(String desc){
            this.desc = desc;
        }
        
        String desc(){
            return desc;
        }
    } 
}
