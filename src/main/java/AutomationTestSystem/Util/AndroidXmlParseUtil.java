package AutomationTestSystem.Util;

import AutomationTestSystem.Base.StepAction;
import AutomationTestSystem.Base.TestCase;
import AutomationTestSystem.Base.TestStep;
import AutomationTestSystem.Base.TestUnit;
import AutomationTestSystem.Service.AndroidXmlParseService;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.*;

public class AndroidXmlParseUtil {

    public static TestUnit parse(File xmlFile) {
        if(xmlFile == null)
            return null;
        try {
            InputStream xmlInputStream = new FileInputStream(xmlFile);
            TestUnit testUnit = parse(xmlInputStream, true);
            return testUnit;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static TestUnit parse(InputStream xmlInputStream,boolean needClose) {
        TestUnit testUnit = null;
        if ( xmlInputStream == null  )
            return testUnit;
        //1.创建Reader对象
        SAXReader reader = new SAXReader();
        try {
            //2.加载xml
            Document document = reader.read(xmlInputStream);


            //3.获取根节点
            Element root = document.getRootElement();

            Iterator casesIt = root.elementIterator();

            //存放case的map
            LinkedHashMap<String, TestCase> caseMap = new LinkedHashMap<String, TestCase>();
            Element child;
            TestCase testCase;

            //逐个解析xml中的case元素
            while (casesIt.hasNext()){
                Element caseElement =   (Element)casesIt.next();

                //testCase = parseTestCase(child);
                testCase = parseLevelTestCase(caseElement);

                if (testCase == null)
                    continue;

                if (caseMap.containsKey(testCase.getId()))
                    throw new RuntimeException("存在多个" + testCase.getId() + "，请检查id配置");
                else
                    caseMap.put(testCase.getId(), testCase);
            }

            testUnit = new TestUnit();
            testUnit.setCaseMap(caseMap);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                if(needClose)
                  xmlInputStream.close();
            }catch (Exception e){
            }
        }
        return testUnit;
    }

    /**
     * br>将case元素解析为TestCase实例，也就是一个TestStep的集合</br>(额外增加支持子集TestStep)
     * @param element 一个testCase
     * @return
     */
    private static TestCase parseLevelTestCase(Element element) {
        if (element == null)
            return null;


        List<Attribute> attrs = element.attributes();
        //根据case的属性实例化TestCase，并注入相应字段值
        TestCase testCase = initByAttributes(attrs, new TestCase());

        List<TestStep> stepList = new ArrayList<TestStep>();

        Iterator stepsIt =   element.elementIterator();
        //逐个解析case元素的step子元素
        while(stepsIt.hasNext()){
            Element stepElement =   (Element) stepsIt.next();

            stepList.add(parseTestStep(stepElement));
        }


        testCase.setSteps(stepList);

        return testCase;
    }

    /**
     * <br>根据xml文件中的元素属性为对象的对应字段注入值</br>
     *
     * @param attrs
     * @param t 需要实例化并注入字段值的对象
     * @return
     */
    private static <T> T initByAttributes(List<Attribute> attrs, T t) {
        if (attrs == null || attrs.isEmpty())
            return t;

        //通过反射逐个注入字段值
        attrs.forEach(a->{
            String name = a.getName();
            String value = a.getValue();
            initByReflect(name, value, t);
        });

        return t;
    }

    /**
     * <br>通过反射为对象的对应字段注入值</br>
     *
     * @param name
     * @param value
     * @param t
     * @return
     */
    private static <T> T initByReflect(String name, String value, T t) {
        if (t == null)
            throw new RuntimeException("null object");
        if (name == null || "".equals(name))
            throw new RuntimeException("empty name");

        Class<?> clazz = t.getClass();
        Method setter, getter;

        try {
            String methodStr = name.substring(0, 1).toUpperCase() + name.substring(1);

            //如果名称是cancel，则调用isCancel()方法，主要是为了语义上的直观
            getter = clazz.getMethod(("cancel".equals(name) ? "is" : "get") + methodStr, new Class<?>[] {});
            setter = clazz.getMethod("set" + methodStr, getter.getReturnType());

            if ("action".equals(name))
                //根据StepAction类中的map来获取名称对应的StepAction（枚举）实例
                setter.invoke(t, StepAction.action(value));
            else if ("cancel".equals(name))
                setter.invoke(t, "true".equals(value) ? true : false);
            else if("details".equals(name))
                setter.invoke(t,parseDetail(value));
            else if("dbIndex".equals(name)){
                setter.invoke(t,Integer.parseInt(value) );
            }
            else
                setter.invoke(t, value);
        } catch (Exception e) {
			System.out.println("对象反射初始化失败");
//			e.printStackTrace();
        }
        return t;
    }

    /**
     * <br>解析行为的细节描述为map</br>
     *
     * @author    102051
     * @date      2017年7月28日 上午11:01:14
     * @param detail
     * @return
     */
    public static Map<String,String> parseDetail(String detail){
        HashMap<String,String> map = new HashMap<>();
        String[] strarr = detail.split(";");

        for(String str : strarr){
            map.put(str.split(":")[0], str.split(":")[1]);
        }
        return map;
    }

    /**
     * <br>解析step元素为TestStep实例</br>
     *
     * @param element
     * @return
     */
    private static TestStep parseTestStep(Element element) {
        if (element == null)
            return null;

        TestStep testStep = initByAttributes(element.attributes(), new TestStep());
        //testStep.setAndroidDriver(AndroidXmlParseService.driver);



        element.elements().size() ;
        if(!element.elements().isEmpty()) {
            testStep.setChildTestStep(new ArrayList());

            Iterator childIt = element.elementIterator();
            while (childIt.hasNext()) {
                Element stepElement = (Element) childIt.next();

                testStep.getChildTestStep().add(parseTestStep(stepElement));
            }
        }

        return testStep;
    }

}
