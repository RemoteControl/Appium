package AutomationTestSystem.Util;

public class Constants {
    
    public static final String CONFIG_JDBC = "appium_jdbc.properties";
    
    public static final String CONFIG_COMMON = "appium_common.properties";
    
    /**
     * 检查条件：列表大小不为空.
     */
    public static final String SIZE_NOT_ZERO = "size_not_null";
    
    /**
     * 检查条件：字段值是否等于预期.
     */
    public static final String FIELD = "field";



    public static final String DB_URL_KEY = "db.url";
    public static final String DB_USERNAME_KEY = "db.username";
    public static final String DB_PASWORD_KEY = "db.password";

    public static final String DB_MAXCONNECT_KEY = "db.maxconnect";
    public static final String DB_INITCONNECT_KEY = "db.initconnect";


}
