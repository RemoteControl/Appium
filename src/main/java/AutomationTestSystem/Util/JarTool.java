package AutomationTestSystem.Util;

import java.io.File;

/**
 * 获取打包后jar的路径信息
 * @author Administrator
 *  2011-01-16 13:53:12
 */
public class JarTool {
    //获取jar绝对路径
    public static String getJarPath(Class yourCLass){
        if(yourCLass == null)
            yourCLass = JarTool.class;
        File file = getFile(yourCLass);
        if(file==null)return null;
        return file.getPath();
    }
    //获取jar目录
    public static String getJarDir(Class yourCLass) {
        if(yourCLass == null)
            yourCLass = JarTool.class;
        File file = getFile(yourCLass);
        if(file==null)
            return null;

        if(file.getName().indexOf(".jar") != -1)
            return file.getParent();
        else
            return file.getPath();
    }
    //获取jar包名
    public static String getJarName(Class yourCLass) {
        if(yourCLass == null)
            yourCLass = JarTool.class;
        File file = getFile(yourCLass);
        if(file==null)return null;
        return getFile(yourCLass).getName();
    }

    private static File getFile(Class yourCLass) {
        //关键是这行...
        String path = yourCLass.getProtectionDomain().getCodeSource()
                .getLocation().getFile();
        try{
            path = java.net.URLDecoder.decode(path, "UTF-8");//转换处理中文及空格
        }catch (java.io.UnsupportedEncodingException e){
            return null;
        }


        if (path.indexOf(".jar!") != -1) {//最后一个去掉“.jar!” 之后的部分
            path = path.substring(0, path.lastIndexOf(".jar!") + ".jar!".length()-1);
        }

       // System.out.println(path);

        return new File(path);
    }

    public static void main(String[] args){
        System.out.println(JarTool.getJarDir(null));

        System.out.println(JarTool.getJarName(null));
    }

}