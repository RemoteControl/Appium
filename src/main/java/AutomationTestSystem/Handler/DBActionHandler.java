package AutomationTestSystem.Handler;

import java.util.List;
import java.util.Map;

import AutomationTestSystem.Service.BaseRunService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import AutomationTestSystem.Base.TestStep;
import AutomationTestSystem.Util.AppiumUtil;
import AutomationTestSystem.Util.DBHelper;
import AutomationTestSystem.Util.SeleniumUtil;

@Slf4j
public class DBActionHandler {

	public void dbQuery(TestStep step) throws Exception{
		if(StringUtils.isBlank(step.getDetails().get("key")))
			throw new Exception("数据库查询务必设置保存结果的键值，供后续操作使用，例子为details='key:credit'！");
		String sql = AppiumUtil.parseStringHasEls(step.getValue());
//		System.err.println("Query-sql "+sql);
		//System.out.println("『正常测试』开始执行: "+ step.getValue() + " <" +step.getDesc() + ">");
		List<Map<String, Object>> st = DBHelper.query(step.getDbIndex(),sql);
		if(st.isEmpty()) {
			log.warn("未查询到数据！");
			step.setMessage("未查询到数据！");
		}
		BaseRunService.localmap.get().put(step.getDetails().get("key"), st);
//		//System.out.println("『正常测试』开始执行: <成功记录到本地List列表，" +AppiumUtil.localmap.toString() + ">");
	}

	public void dbUpdate(TestStep step) throws Exception{
		//System.out.println("『正常测试』开始执行: "+ step.getValue() + " <" +step.getDesc() + ">");
		String sql = AppiumUtil.parseStringHasEls(step.getValue());
//		System.err.println("Query-update "+sql);
		int n = DBHelper.update(step.getDbIndex(),sql);
		if(n > 0){
			//System.out.println("『正常测试』开始执行: "+ step.getValue() + " <" +step.getDesc() + ">");
//			Reporter.log(base.getDesc());
		}else {
			log.warn("未更新到数据！");
			step.setMessage("未更新到数据");
		}
	}


	public void dbDelete(TestStep step) throws Exception{
		//System.out.println("『正常测试』开始执行: "+ step.getValue() + " <" +step.getDesc() + ">");
		int n = DBHelper.delete(step.getDbIndex(),AppiumUtil.parseStringHasEls(step.getValue()));
		if(n > 0){

		}else {
			log.warn("未删除到数据");
			step.setMessage("未删除到数据");
		}
	}


	public void dbInsert(TestStep step) throws Exception{
		//System.out.println("『正常测试』开始执行: "+ step.getValue() + " <" +step.getDesc() + ">");
		int n = DBHelper.insert(step.getDbIndex(),AppiumUtil.parseStringHasEls(step.getValue()));
		if(n > 0){

		}else{
			log.warn("未保存成功（保存0条）");
			step.setMessage("未保存成功（保存0条）");
		}

	}


	public void dbProcedure(TestStep step) throws Exception{
		String params = AppiumUtil.parseStringHasEls(step.getValue());
		//System.out.println("『正常测试』开始执行: "+ step.getDetails().get("prc_name") + params + " <" +step.getDesc() + ">");
		int n =DBHelper.procedure(step.getDbIndex(),step.getDetails().get("prc_name"),params,null);
//		if(n > 0){
//
//		}else{
//			log.warn("未保存成功（保存0条）");
//			step.setMessage("未保存成功（保存0条）");
//		}
	}
	

}
