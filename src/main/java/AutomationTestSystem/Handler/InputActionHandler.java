package AutomationTestSystem.Handler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import AutomationTestSystem.Service.BaseRunService;
import AutomationTestSystem.Util.HttpPostRequestUtil;
import AutomationTestSystem.exception.BusinessException;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.google.common.base.Splitter;
import io.restassured.path.json.JsonPath;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.WebElement;

import AutomationTestSystem.Base.TestStep;
import AutomationTestSystem.Util.AppiumUtil;
import AutomationTestSystem.Util.SeleniumUtil;

public class InputActionHandler {
	//提取手机号后返回的流水号
	public static final String INPUTE_MOBILE_ORDERNO = "INPUTE_MOBILE_ORDERNO";
	public static final String INPUTE_MOBILE_CHECKCODE = "INPUTE_MOBILE_CHECKCODE";
	public static final String INPUTE_MOBILE = "INPUTE_MOBILE";

	/**
	 * <br>Web端输入操作</br>
	 *
	 * @param step
	 * @throws Exception 
	 */
	public void webInput(TestStep step) throws Exception{ 
		//System.out.println("『正常测试』开始执行: " + "<" +step.getDesc() + ">");
		step.getWebDriver().manage().timeouts().implicitlyWait(step.getFinalImplicitlyWait(), TimeUnit.MILLISECONDS);
		WebElement e = SeleniumUtil.getElement(step,true);
		e.clear();
		e.sendKeys(SeleniumUtil.parseStringHasEls(step.getValue()));	
	}
	
	/**
	 * <br>Android端输入操作</br>
	 *
	 * @param step
	 * @throws Exception 
	 */
	public void androidInput(TestStep step) throws Exception{ 
		//System.out.println("『正常测试』开始执行: " + "<" +step.getDesc() + ">");
		step.getAndroidDriver().manage().timeouts().implicitlyWait(step.getFinalImplicitlyWait(), TimeUnit.MILLISECONDS);
		WebElement e = AppiumUtil.getElement(step,true);
		e.clear();
		e.sendKeys(AppiumUtil.parseStringHasEls(step.getValue()));	
	}

	/**
	 * 获取手机号,本地Map 放置流水：
	 * <step action="android-input-mobile" locator="text=收入手机号" url="http://sms.mingshenggroup.cn:9090/cardCharge/receiveCode" body="userId=2566&itemId=3414&needReady=1" desc="提取手机号" details="key:提取秘钥;delayTime:10"  value="uid" />
	 * @param step
	 * @return
	 * @throws Exception
	 */
	public void androidInputMobile(TestStep step) throws Exception{
		//System.out.println("『正常测试』开始执行: " + "<" +step.getDesc() + ">");
		String ApiUrl = step.getUrl();
		String Param = step.getBody();
		String value =	step.getValue();
		String key = step.getDetails().get("key");
		String delayTime = step.getDetails().get("delayTime");//延时后执行查询
		if(StrUtil.isEmpty(delayTime) || NumberUtil.isInteger(delayTime)){
			delayTime = "10";
		}


		if(StrUtil.isEmpty(value))
			value = "uid";
		//userId=2566&itemId=3414&needReady=1
		List<String> paramsStr = Splitter.on("&").splitToList(Param);
		Map<String,String> params =  new HashMap();
		for(String one:paramsStr){
			if(StrUtil.isNotEmpty(one) && one.indexOf("=")!= -1){
				String[] temps =	one.split("=");

				params.put(temps[0],temps[1]);
			}
		}

		params.put("serialno",String.valueOf(System.currentTimeMillis()) );
		params.put("dtCreate", DateUtil.now());

		if (StrUtil.isNotEmpty(key)) {
			String src = HttpRequestHandler.mapSortedByKey(params) + key;
			System.out.println("获取手机号src:" + src);
			String after = cn.hutool.crypto.digest.MD5.create().digestHex(src);
			params.put("sign", after);
		}else
			System.out.println("<" +step.getDesc() + ">  无秘钥不签名！" );

		String mobile = HttpPostRequestUtil.GetTransJsonValueValue(ApiUrl,params,value);
		BaseRunService.localmap.get().put(INPUTE_MOBILE,mobile);

		String srcOrderNo = params.get("serialno");

		//流水号保存，后续要用
		BaseRunService.localmap.get().put(INPUTE_MOBILE_ORDERNO, srcOrderNo);

		step.getAndroidDriver().manage().timeouts().implicitlyWait(step.getFinalImplicitlyWait(), TimeUnit.MILLISECONDS);
		WebElement e = AppiumUtil.getElement(step,true);
		e.clear();
		e.sendKeys(AppiumUtil.parseStringHasEls(mobile));

		{
			//执行查询，不过得等后面设置一个值(还是自动延时执行？自动化 输入值后肯定会点击发码)
			AtomicInteger flag = BaseRunService.localVar.get();
			int delaySecond = Integer.parseInt(delayTime);
			long beginTime = System.currentTimeMillis();
			int endTime = 120;//默认120秒吧，后面的会退出的

			Map localMap = BaseRunService.localmap.get();
			String srcOrderNum = (String)localMap.get(INPUTE_MOBILE_ORDERNO);

			new Thread(() -> {

				ThreadUtil.safeSleep(delaySecond*1000);//10秒后开始查询
				//http://sms.mingshenggroup.cn:9090/cardCharge/queryBizOrder
				String queryUrl = ApiUrl.substring(0,ApiUrl.indexOf("/",ApiUrl.indexOf("//")+2)) + "/cardCharge/queryBizOrder";
				System.out.println("查询地址："+queryUrl);
				while (flag.get() == 1 && (System.currentTimeMillis() - beginTime)/1000<endTime) {//启动中
					{

						Map<String,String> queryParams =  new HashMap();
						queryParams.put("userId",params.get("userId"));
						queryParams.put("serialno",srcOrderNum);

						if (StrUtil.isNotEmpty(key)) {
							String src = HttpRequestHandler.mapSortedByKey(queryParams) + key;
							System.out.println("查询src:" + src);
							String after = cn.hutool.crypto.digest.MD5.create().digestHex(src);
							queryParams.put("sign", after);
						}else
							System.out.println("<" +step.getDesc() + ">  查询无秘钥不签名！" );

						try {
							String json = HttpPostRequestUtil.GetTransJsonValue(queryUrl,queryParams);
							JsonPath path =	new JsonPath(json);
							String code = (path).getString("code");//00表示查询成功
							String status = (path).getString("data.status");
							String memo = (path).getString("data.memo");//短信内容

							if("00".equals(code)){
								if("2".equals(status) &&StrUtil.isNotEmpty(memo)){
									//【抖音】验证码#code#，用于手机登录，5分钟内有效。验证码提供给他人可能导致帐号被盗，请勿泄露，谨防被骗。
									System.out.println("接收到短信："+memo);

									String msgCode = AppiumUtil.getYzmFromSms(memo) ;
									if(StrUtil.isNotEmpty(code))
										localMap.put(INPUTE_MOBILE_CHECKCODE,msgCode);

									break;
								}
							}


						} catch (BusinessException businessException) {
							businessException.printStackTrace();
						}
						ThreadUtil.safeSleep(2000);//2秒查询一次
					}
				}

			}).start();

		}

	}

	/**
	 * 从Map设置值 （直到有值，否则提示超时退出）
	 * INPUTE_MOBILE_CHECKCODE 验证码
	 * <step action="android-input-by" details="key:checkCode"  locator="resource-id= com.giveu.corder:id/tv_contractNo" details="key:checkCode;maxTime:120"  desc="从Map设置值" />
	 * @param step
	 * @return
	 * @throws Exception
	 */
	public void androidInputBy(TestStep step) throws Exception{
		//System.out.println("『正常测试』开始执行: " + "<" +step.getDesc() + ">");
		if(StringUtils.isBlank(step.getDetails().get("key")))
			throw new Exception("操作必须获取保存结果的键值，以提取key值，例子为details='key:credit'！");

		String key = step.getDetails().get("key");


		String maxTime = (String)step.getDetails().get("maxTime");
		if(StrUtil.isEmpty(maxTime) && !NumberUtil.isInteger(maxTime)){
			maxTime = "120";
		}

		long c = System.currentTimeMillis();
		//long endTime = c + Integer.parseInt(maxTime)*1000L;

		int lastTime = Integer.parseInt(maxTime)+5;


		while((System.currentTimeMillis() - c)/1000<lastTime){
			Object keyValue = BaseRunService.localmap.get().get(key);
			if(keyValue != null){
				WebElement e = AppiumUtil.getElement(step,true);
				e.clear();
				e.sendKeys(AppiumUtil.parseStringHasEls(keyValue.toString()));
				return;
			}else{
				System.out.println(step.getDesc()+" 又没取到值~~");
			}
			ThreadUtil.safeSleep(1500);
		}

		throw new BusinessException(step.getDesc()+" 最终未等到值！");


	}






	public static void main(String[] args){
		//http://sms.mingshenggroup.cn:9090/cardCharge/queryBizOrder

		String ApiUrl = " http://sms.mingshenggroup.cn:9090/cardCharge/receiveCode";

		String queryUrl = ApiUrl.substring(0,ApiUrl.indexOf("/",ApiUrl.indexOf("//")+2)) + "/cardCharge/queryBizOrder";
		System.out.println(queryUrl);

		String json = "{\"code\":\"11\",\"desc\":\"校验码验证失败\",\"status\":\"failed\",\"data\":{\"status\":\"1\"}}";
		JsonPath path =	new JsonPath(json);
		String code = (path).getString("code");//00表示查询成功
		String status = (path).getString("data.status");

		System.out.println(code);
		System.out.println(status);
	}
}
