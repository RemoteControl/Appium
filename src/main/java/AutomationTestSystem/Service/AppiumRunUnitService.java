package AutomationTestSystem.Service;

import AutomationTestSystem.Base.TestCase;
import AutomationTestSystem.Base.TestUnit;
import AutomationTestSystem.Util.ConfigUtil;
import cn.hutool.core.util.StrUtil;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.concurrent.ConcurrentHashMap;

public class AppiumRunUnitService implements BaseRunService{
   private TestUnit testunit;
   private AndroidDriver<WebElement> driver;
   private Integer runMode;

   public AppiumRunUnitService(AndroidDriver<WebElement> driver,TestUnit testunit){
        this.driver = driver;
        this.testunit = testunit;
        this.runMode = 1;

       BaseRunService.localmap.set(new ConcurrentHashMap<>());

   }

    /**
     *
     * @param driver
     * @param testunit
     * @param configPath 默认配置文件路径
     */
    public AppiumRunUnitService(AndroidDriver<WebElement> driver,TestUnit testunit,String configPath){
        this.driver = driver;
        this.testunit = testunit;
        this.runMode = 1;

        BaseRunService.localmap.set(new ConcurrentHashMap<>());
        if(StrUtil.isNotEmpty(configPath) &&  StrUtil.isEmpty(ConfigUtil.DEFAULT_CONFIG_PATH))
         ConfigUtil.DEFAULT_CONFIG_PATH = configPath;
    }


    @Override
    public LinkedHashMap<String, TestCase> getCases() {
        return testunit.getCaseMap();
    }

    @Override
    public TestCase getCase(String id) {
        return testunit.getCaseMap() == null ? null : testunit.getCaseMap().get(id);
    }

    @Override
    public void close() {
        BaseRunService.localmap.remove();
        BaseRunService.localVar.remove();
       this.getDriver().closeApp();

        System.out.println("『测试结束』开始执行: <退出App>");
    }

    @Override
    public Integer getRunMode() {
        return this.runMode;
    }

    @Override
    public AndroidDriver<WebElement> getDriver() {
        return driver;
    }
}
