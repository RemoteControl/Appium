package AutomationTestSystem.Service;

import AutomationTestSystem.Base.StepAction;
import AutomationTestSystem.Base.TestCase;
import AutomationTestSystem.Base.TestStep;
import AutomationTestSystem.Base.TestUnit;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public interface BaseRunService {
    static ThreadLocal<AtomicInteger> localVar = new ThreadLocal();
    public static final String UNIT_CASE_FALG = "UNIT_CASE_FALG";

    //线程内step变量
    public static ThreadLocal<Map<String,Object>> localmap = new ThreadLocal();//new HashMap<>();

    /**
     * 系统默认的 隐式等待(秒)
     */
    public static int implicitlyWait = 5;

    /**
     *获取全部案例
     */
    public LinkedHashMap<String,TestCase> getCases();

    /**
     * 获取案例
     * @param id
     * @return
     */
    public TestCase getCase(String id);


    /**
     * 退出
     */
    public void close() ;

    /**
     * 运行类型：1=android ,2=web
     * @return
     */
    public Integer getRunMode();

    /**
     * 获取驱动
     * @return
     */
    public Object getDriver();


    /**
     * <br>执行测试用例</br>
     *
     * @param id
     * @throws Exception
     */
    default public void runCase(String id) throws Exception{
        AtomicInteger startFlag = new AtomicInteger(1);
        localVar.set(startFlag);
        TestCase testCase = null;
        try {
            testCase = getCase(id);
            System.out.println("-----------------------------------测试案例：【"+testCase.getName()+"】 开始-----------------------------------");

            List<TestStep> steps = testCase.getSteps();

            runSteps(steps);


            TestReportRemarks(testCase.getName());


        }finally {
            System.out.println("-----------------------------------测试案例：【"+testCase.getName()+"】 完成-----------------------------------");
            localVar.remove();

            //

        }
    }



    default void runSteps(List<TestStep> steps) throws Exception{
        for(TestStep step : steps){
            if(step.isCancel())
                continue;
            //step.getAction()
            System.out.println("『开始执行』: "+step.getDesc());


            if(this.getRunMode() == 1){
                step.setAndroidDriver((AndroidDriver<WebElement>)this.getDriver());
            }else if(this.getRunMode() == 1){
                step.setWebDriver((WebDriver)this.getDriver());
            }



            StepAction action = step.getAction();
            Class<?> clazz = action.handler();

            //如果StepAction实例有handler字段，则调用handler中的方法，否则直接调用run方法
            if(clazz != null){
                String key = step.getAction().key();
                Method m = clazz.getDeclaredMethod(getMethodName(key), new Class<?>[]{TestStep.class});

                Type t = m.getReturnType();
                Type returnType = m.getGenericReturnType();
                try {
                    if (t.getTypeName().equalsIgnoreCase("boolean")) {
                        boolean result = (boolean) m.invoke(clazz.newInstance(), step);

                        System.out.println("『执行完成』: " + step.getDesc());
                        if (result) {//成功执行下属节点
                            runSteps(step.getChildTestStep());
                        } else {
                            System.out.println("条件未达成，跳过此节点:<" + step.getDesc() + "> 下属！");
                        }
                        step.setMessage(String.valueOf(result));
                    } else {
                        m.invoke(clazz.newInstance(), step);
                    }
                    //java.lang.reflect.InvocationTargetException
                    System.out.println("『执行完成』: " + step.getDesc());

                    step.setResult(1);
                }catch (java.lang.reflect.InvocationTargetException e){

                    System.out.println("『执行』: "+step.getDesc()+",异常："+e.getTargetException().getMessage());
                    step.setResult(0);
                    step.setMessage(e.getTargetException().getMessage());
                    throw e;
                }
            }else{
                action.run(step);

            }
        }
    }


    /**
     * <br>根据step元素的值解析出对应的方法名</br>
     * 作用是将"-"后面的第一个字母转为大写，并且去掉“-”
     *
     * @param actionKey
     * @return
     */
    default public String getMethodName(String actionKey){
        if(actionKey == null || "".equals(actionKey))
            throw new RuntimeException("empty action key");

        char[] arr = actionKey.toCharArray();
        char prevChar = '\0';
        StringBuilder sb = new StringBuilder();
        char splitChar = '-';

        for(char c : arr){
            if(c == splitChar){
                prevChar = c;
                continue;
            }
            if(prevChar == splitChar) {
                sb.append(Character.toUpperCase(c));
            } else {
                sb.append(c);
            }
            prevChar = c;
        }

        return sb.toString();
    }

    /**
     * <br>标记备注，一般展示在测试报告中</br>
     *
     * @param RemarksName
     */
    default void TestReportRemarks(String RemarksName){
        Reporter.log(RemarksName);
    }
}
