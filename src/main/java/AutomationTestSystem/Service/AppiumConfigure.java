package AutomationTestSystem.Service;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * appium参数
 * otherParams 参水会被之前的参数覆盖掉（如果设置之前参数的话）
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class AppiumConfigure {
    private String deviceID;

    /**
     * 平台名称，默认Android
     */
    @Builder.Default
    private String platformName ="Android";

    /**
     * 平台版本
     */
    private String platformVersion;

    /**
     * 是否重置app信息
     * noReset=false 为重置
     */
    @Builder.Default
    private boolean noReset = true;

    /**
     * 重新安装APP，true(重新安装)/false(不重新安装) 默认false
     */
    @Builder.Default
    private boolean fullReset = false;


    /**
     * 包名
     */
    private String apkPackage;
    /**
     * activity
     */
    private String apkActivity;

    /**
     * 安装包路径（无app就安装）
     */
    private String apkPath;

    /**
     * 开启中文输入，安装Unicode输入法，true(安装)/false(不安装)
     */
    @Builder.Default
    private boolean unicodeKeyboard = true;

    /**
     *还原系统默认输入法，true(还原)/false(不还原) ,手机虚拟键盘不见（不见也有好处输入的时候减少麻烦）
     */
    @Builder.Default
    private boolean resetKeyboard = true;



    /**
     * 其他参数，不支持设置以上的参会，会被覆盖
     */
    private Map<String,Object> otherParams ;

}
