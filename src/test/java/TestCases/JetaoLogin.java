package TestCases;

import AutomationTestSystem.Base.TestUnit;
import AutomationTestSystem.Service.AndroidXmlParseService;
import AutomationTestSystem.Service.AppiumRunUnitService;
import AutomationTestSystem.Util.AndroidXmlParseUtil;
import AutomationTestSystem.Util.ConfigUtil;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.File;

public class JetaoLogin {
	
	private  AppiumRunUnitService runService;
		
	@BeforeTest
	private void stup() throws Exception{
//		TestUnit testunit = AndroidXmlParseService
//				.parse("d:\\jet.apk","com.tencent.mm","Android","5.1.1"
//						,"1ed814f6","TestCaseXml/WeChatLogin.xml");

		/**
		 * 小米8
		 */
		String DeviceID ="ff079e85";
		String PlatformVersion = "10";

		 DeviceID ="M7BBB19102200043";
		PlatformVersion = "8.1.0";


		AndroidDriver<WebElement> driver = AndroidXmlParseService.AppiumConfigure2("com.jiuetao.merchant","io.dcloud.PandoraEntry"
				,"Android",PlatformVersion,DeviceID);

		TestUnit testunit = AndroidXmlParseUtil.parse(new File("src/test/java/"+"TestCaseXml/Jetao.xml"));


		runService = new AppiumRunUnitService(driver,testunit);
		System.out.println("-----------------------------------【酒易淘登录流程的测试场景点】-----------------------------------");
	}
	
	//@Test
	public void case1() throws Exception{
		runService.runCase("caseJumpWel");
	}

	//@Test
	public void case2() throws Exception{
		runService.runCase("case2");
	}

	//@Test
	public void case3() throws Exception{
		runService.runCase("case3");
	}

	//@Test
	public void case13() throws Exception{
		runService.runCase("caseJumpWel");
		runService.runCase("case3");
	}

	@Test
	public void caseDb() throws Exception{
		runService.runCase("caseDb");
	}
	
	@AfterTest
	public void TearDown(){
		runService.close();
	}
}
