package TestCases;

import AutomationTestSystem.Service.AppiumRunUnitService;
import AutomationTestSystem.Util.AndroidXmlParseUtil;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import AutomationTestSystem.Base.TestUnit;
import AutomationTestSystem.Service.AndroidXmlParseService;

import java.io.File;

public class WeChatLogin {

	private AppiumRunUnitService runService;
		
	@BeforeTest
	private void stup() throws Exception{
//		TestUnit testunit = AndroidXmlParseService
//				.parse("WeChat.apk","com.tencent.mm","Android","8.1.0","1ed814f6","TestCaseXml/WeChatLogin.xml");



		AndroidDriver<WebElement> driver = AndroidXmlParseService
				.AppiumConfigure2("d:\\jet.apk","com.tencent.mm","Android","5.1.1"
						,"1ed814f6");

		TestUnit testunit = AndroidXmlParseUtil.parse(new File("src/test/java/"+"TestCaseXml/WeChatLogin.xml"));


		runService = new AppiumRunUnitService(driver,testunit);

		//System.out.println("-----------------------------------【微信登录流程的测试场景点】-----------------------------------");
	}
	
	//@Test
	public void case1() throws Exception{
		runService.runCase("case1");
	}

	@Test
	public void case2() throws Exception{
		runService.runCase("case2");
	}
	
	@AfterTest
	public void TearDown(){
		runService.close();
	}
}
